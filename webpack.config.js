const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const webpack = require("webpack");
const alias = {
  assets: path.resolve(__dirname, "src/assets/"),
  components: path.resolve(__dirname, "src/components/")
};

// 提供图片，css等等静态资源
const staticFilePath = path.resolve(__dirname, "dist");

const rootDir = path.resolve(__dirname);
module.exports = {
  mode: "development",
  entry: ["@babel/polyfill", "./src/index.js"],
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "dist"),
    publicPath: "/"
  },
  devtool: "inline-source-map",
  devServer: {
    contentBase: staticFilePath,
    hot: true,
    inline: true, // 默认配置 自动在bundle.js 插入js, 负责build reloading and show message in console
    compress: true, // 使用gzip压缩
    // stats: "minimal",
    port: 8080,
    publicPath: "/", // where the bundles should be served from
    host: "0.0.0.0", // make server accessible externally
    historyApiFallback: true, // 在开发单页应用时非常有用，它依赖于HTML5 history API，如果设置为true，所有的跳转将指向index.html
    proxy: {
      "/api": "https://3yg3c5fwo6.execute-api.us-east-1.amazonaws.com/prod"
    }
  },
  resolve: {
    alias: alias
  },
  module: {
    rules: [
      {
        // enforce: 'pre',
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader", "postcss-loader"]
      },
      {
        test: /\.less$/,
        use: [
          {
            // loader: MiniCssExtractPlugin.loader // creates style nodes from JS strings
            loader: "style-loader"
          },
          {
            loader: "css-loader", // translates CSS into CommonJS
            options: {
              // modules: true,// 开启
              sourceMap: true
              // localIdentName: '[name]__[local]___[hash:base64:5]'
            }
          },
          "postcss-loader",
          {
            loader: "less-loader" // compiles Less to CSS
          }
        ]
      },

      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ["file-loader"]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              outputPath: "assets" // 指定打包后的图片位置
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "css/[name].css",
      chunkFilename: "css/[id].css"
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "./src/index.html"),
      inject: true
      // favicon: path.resolve(__dirname, '../src/asset/images/favicon.ico'),
    }),
    new webpack.HotModuleReplacementPlugin(),
    new CopyPlugin([
      {
        from: path.resolve(rootDir, "src/assets"),
        to: path.resolve(rootDir, "dist/assets")
      }
    ])
  ]
};
