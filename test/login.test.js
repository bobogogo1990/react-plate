import { Selector } from "testcafe";
import { ReactSelector } from "testcafe-react-selectors";

fixture`Iot App test`.page`http://localhost:8080/login`;

test("test username and password", async t => {
  const inputs = ReactSelector("Login Input");
  const password = ReactSelector("Login Password Input");
  const login = ReactSelector("Login");

  const username = inputs.nth(0);

  // test dom value
  await t
    .typeText(username, "test1")
    .expect(username.value)
    .eql("test1");

  await t.typeText(password, "123456");

  const { state = {} } = await login.getReact();

  // test state value
  await t.expect(state.username).eql("test1");
  await t.expect(state.password).eql("123456");
});
