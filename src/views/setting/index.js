import React from "react";
import Breadcrumb from "components/breadcrumb";
import { Empty } from 'antd';

export default class Settings extends React.Component {
    state = {
    };

    async componentDidMount() {

    }

    render() {
        return <div>
            <Breadcrumb activeLink={"Automotive"} />
            <Empty description={"Temporarily unable to set, please be patient for the next version🤦‍♀️"} />
        </div>
    }
}
