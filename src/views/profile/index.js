import React from "react";
import ProfileInfo from "./ProfileInfo.js";
import Breadcrumb from "components/breadcrumb";
import { Form, Input, Button, message } from "antd";

class Profile extends React.Component {
  state = {
    confirmDirty: false,
    profile: {},
    old: "",
    current: "",
    recurrent: "",
    pwsame: false
  };

  handleSubmit = async e => {
    e.preventDefault();
    const form = this.props.form;
    const { old, current, recurrent } = form.getFieldsValue();
    if (current !== recurrent) {
      // show message
      return;
    }
    try {
      const res = await aws.changePassword(old, current);
      form.setFieldsValue({ old: "", current: "", recurrent: "" });
      message.success("password update success~");
    } catch (error) {
      console.log("error = ", error);
      message.error("password update failed~");
    }
  };
  handleConfirmBlur = e => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    const current = form.getFieldValue("current");
    if (!value || !current) {
      this.setState({ pwsame: false });
    }
    if (value && value !== current) {
      this.setState({ pwsame: false });
      callback("Does not match your new password.");
    } else {
      this.setState({ pwsame: true });
      callback();
    }
  };
  validateCurrentPassword = (rule, value, callback) => {
    const form = this.props.form;
  };
  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(["confirm"], { force: true });
    }
    callback();
  };

  oldChange = ({ target }) => {
    this.props.form.setFieldsValue({
      old: target.value
    });
  };

  currentChange = ({ target }) => {
    this.props.form.setFieldsValue({
      current: target.value
    });
  };

  recurrentChange = ({ target }) => {
    this.props.form.setFieldsValue({
      recurrent: target.value
    });
  };

  componentDidMount() {
    const profile = aws.getLocalData("profile");
    this.setState({
      profile: profile && profile != "undefined" ? JSON.parse(profile) : {}
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Breadcrumb activeLink={"My Profile"} />
        <div className="row">
          <div className="col-lg-12">
            <ProfileInfo profile={this.state.profile} />
            <div className="card" style={{ padding: "1.25rem" }}>
              <h4 className="card-title">Change your password</h4>
              <h6 className="card-subtitle">
                Change your password by entering your old password for
                verification and your desired new password.
              </h6>
              <div className="alert alert-info">
                <h3 className="text-info">
                  <i className="fa fa-exclamation-circle" /> Password complexity
                  requirements
                </h3>{" "}
                Password must be at least 8 characters, and must include at
                least one upper case letter, one lower case letter, and one
                numeric digit.
              </div>
              <Form formitemlayout="vertical" onSubmit={this.handleSubmit}>
                <Form.Item label="Current Password:">
                  {getFieldDecorator("old", {
                    rules: [
                      {
                        required: true,
                        message: "Your current password is required."
                      },
                      {
                        validator: this.validateCurrentPassword
                      }
                    ]
                  })(<Input type="password" onChange={this.oldChange} />)}
                </Form.Item>
                <Form.Item label="New Password:">
                  {getFieldDecorator("current", {
                    rules: [
                      {
                        required: true,
                        message: "Your new password is required."
                      },
                      {
                        validator: this.validateToNextPassword
                      }
                    ]
                  })(<Input type="password" onChange={this.currentChange} />)}
                </Form.Item>
                <Form.Item label="Confirm New Password:">
                  {getFieldDecorator("recurrent", {
                    rules: [
                      {
                        required: true,
                        message: "Does not match your new password."
                      },
                      {
                        validator: this.compareToFirstPassword
                      }
                    ]
                  })(
                    <Input
                      type="password"
                      onBlur={this.handleConfirmBlur}
                      onChange={this.recurrentChange}
                    />
                  )}
                </Form.Item>
                <Form.Item>
                  <Button>Cancel</Button>
                  <Button
                    type="primary"
                    style={{ marginLeft: 18 }}
                    htmlType="submit"
                    disabled={!this.state.pwsame}
                  >
                    Update Password
                  </Button>
                </Form.Item>
              </Form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
Profile = Form.create({})(Profile);

export default Profile;
