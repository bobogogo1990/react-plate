import React from "react";

export default class ProfileInfo extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { profile = {} } = this.props;
    return (
      <div className="card">
        <div className="card-body">
          <h4 className="card-title">Profile Information</h4>
          <h6 className="card-subtitle">
            Your application user account information. Please contact your
            administrator for further information.
          </h6>
          <form className="form-material m-t-40">
            <div className="form-group">
              <label>
                <b>User Id</b>
              </label>
              <p>{profile.user_id}</p>
            </div>
            <div className="form-group">
              <label>
                <b>User Name</b>
              </label>
              <p>{profile.name}</p>
            </div>
            <div className="form-group">
              <label>
                <b>User Email</b>
              </label>
              <p>{profile.email}</p>
            </div>
            <div className="form-group">
              <label>
                <b>Group(s)</b>
              </label>
              <p>{profile.groups}</p>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
