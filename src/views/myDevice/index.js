import React from "react";
import "./index.less";
import Breadcrumb from "components/breadcrumb";
import { Pagination } from 'antd';
import dayjs from "dayjs";

class Device extends React.Component {
    state = {
        metrics: null,
        widgetList: null
    };

    async componentDidMount() {
        const widgets = await awsapi.deviceWidgets({
            op: "stats"
        });
        const widgetList = await awsapi.deviceWidgets({
            page: 0,
            op: "list"
        });
        this.setState({ metrics: widgets, widgetList });
    }
    loadDeviceTypes() {
        alert("refresh")
    }
    refreshData() {
        this.loadDeviceTypes();
    }
    render() {
        const { metrics, widgetList } = this.state;
        return <div>
            <Breadcrumb activeLink={"Devices"} />
            <div className="row">
                <div className="col-lg-3 col-md-6">
                    <div className="card">
                        <div className="card-body">
                            <div className="d-flex flex-row">
                                <div className="round round-lg align-self-center round-muted"><i className="mdi mdi-chip"></i></div>
                                <div className="m-l-10 align-self-center">
                                    <h3 className="m-b-0 font-light">{metrics ? metrics.total : ""}</h3>
                                    <h5 className="text-muted m-b-0">My Total Devices</h5></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-md-6">
                    <div className="card">
                        <div className="card-body">
                            <div className="d-flex flex-row">
                                <div className="round round-lg align-self-center round-success-alt"><i className="mdi mdi-chip"></i></div>
                                <div className="m-l-10 align-self-center">
                                    <h3 className="m-b-0 font-lgiht">{metrics ? metrics.hydrated : ""}</h3>
                                    <h5 className="text-muted m-b-0">Simulating</h5></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-md-6">
                    <div className="card">
                        <div className="card-body">
                            <div className="d-flex flex-row">
                                <div className="round round-lg align-self-center round-warning"><i className="mdi mdi-chip"></i></div>
                                <div className="m-l-10 align-self-center">
                                    <h3 className="m-b-0 font-lgiht">{metrics ? metrics.provisioning : ""}</h3>
                                    <h5 className="text-muted m-b-0">Provisioning</h5></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-md-6">
                    <div className="card">
                        <div className="card-body">
                            <div className="d-flex flex-row">
                                <div className="round round-lg align-self-center round-danger"><i className="mdi mdi-chip"></i></div>
                                <div className="m-l-10 align-self-center">
                                    <h3 className="m-b-0 font-lgiht">{metrics ? metrics.sleeping : ""}</h3>
                                    <h5 className="text-muted m-b-0">Sleeping</h5></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <div className="card">
                        <div className="card-body">
                            <button type="button"
                                className="float-right btn btn-sm btn-success m-l-15"
                                onClick={this.refreshData} >
                                <i className="fa fa-refresh"></i> Refresh
                            </button>
                            <h4 className="card-title">Devices</h4>
                            <div className="table-responsive m-t-20">
                                <table id="devices-pagination" className="table stylish-table" style={{ textTransform: "uppercase" }}>
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Device</th>
                                            <th className="text-right">Stage</th>
                                            <th className="text-right">Simulations</th>
                                            <th className="text-right">Created</th>
                                            <th className="text-right">Last Simulation</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            widgetList && widgetList.length && widgetList.map((v, i) => {
                                                return < tr key={i}>
                                                    <td>
                                                        <span className="round round-light-grey">
                                                            <i className="mdi mdi-chip text-normal"></i>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <h6>{v.id}</h6><small className="text-muted">{v.category} ({v.subCategory})</small>
                                                    </td>
                                                    <td className="text-right">
                                                        <span className={v.stage === 'hydrated' ? 'label label-light-success-alt' : (v.stage === 'provisioning' ? 'label label-light-warning' : 'label label-light-danger')}>{v.stage === 'hydrated' ? 'running' : v.stage}</span>
                                                    </td>
                                                    <td className="text-right"><span className="label label-success">{v.runs}</span></td>
                                                    <td className="text-right">{dayjs(v.createdAt).format("MMM DD YYYY")}</td>
                                                    <td className="text-right">{dayjs(v.lastRunAt).format("MMM DD YYYY")}</td>
                                                    <td>
                                                        <a class="pull-right btn btn-sm btn-success btn-rounded" href={v.category === 'automotive' ? '/securehome/automotive/vehicle/' + v.id : '/securehome/general/' + v.id}><i class="mdi mdi-information-outline"></i> View</a>
                                                    </td>
                                                </tr>
                                            })
                                        }
                                    </tbody>
                                </table>
                                <Pagination defaultCurrent={1} total={50} style={{ float: "right" }} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    }
}
export default Device;
