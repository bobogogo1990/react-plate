import React from "react";
import "./index.less";
import Breadcrumb from "components/breadcrumb";
import { message } from "antd";

export default class NewDeviceType extends React.Component {
  state = {
    name: "",
    visibility: "private",
    topic: "/sample/topic",
    duration: 60000,
    interval: 2000
  };

  nameChange = ({ target }) => {
    this.setState({ name: target.value });
  };

  visibilityChange = e => {
    this.setState({ visibility: e.target.value });
  };

  topicChange = ({ target }) => {
    this.setState({ topic: target.value });
  };

  durationChange = ({ target }) => {
    this.setState({ duration: target.value });
  };

  intervalChange = ({ target }) => {
    this.setState({ interval: target.value });
  };

  async componentDidMount() {}

  createDeviceType = async () => {
    const { name, duration, interval, topic, visibility } = this.state;
    // 请求参数
    const data = {
      custom: true,
      name,
      spec: {
        duration,
        interval,
        payload: [],
        topic
      },
      typeId: "",
      visibility
    };

    try {
      // 新增设备类型
      const one = await awsapi.createDeviceType(data);
      console.log("one = ", one);
      message.success("新增成功");
      aws.pushWindow(this, "/securehome/deviceType");
    } catch (error) {
      console.log("错误提示 = ", error);
      message.error("新增失败");
    }
  };

  render() {
    const { name, duration, interval, topic, visibility } = this.state;
    return (
      <div>
        <Breadcrumb activeLink={"DeviceType"} />
        <div className="row">
          <div className="col-lg-12">
            <div className="card">
              <div className="card-body">
                <h4 className="card-title">
                  <span>Device Type Definition</span>
                  <span
                    className={
                      visibility === "shared"
                        ? "label label-light-success-alt pull-right"
                        : "label label-light-danger pull-right"
                    }
                  >
                    {visibility}
                  </span>
                </h4>
                <h6 className="card-subtitle">
                  Customize how device data of this customized type is sent to
                  AWS IoT.
                </h6>
                <form className="fm-t-40">
                  <div className="form-group">
                    <label>Device Type Name</label>
                    <input
                      type="text"
                      className="form-control form-control-line"
                      name="typename"
                      value={name}
                      onChange={this.nameChange}
                      id="typename"
                    />
                    <div className="form-control-feedback">
                      <small>The common name of the device type.</small>
                    </div>
                  </div>
                  <div className="form-group">
                    <label className="form-control-label">Visibility:</label>
                    <select
                      className="custom-select col-12"
                      name="visibility"
                      id="visibility"
                      value={visibility}
                      onChange={this.visibilityChange}
                    >
                      <option value="private">PRIVATE</option>
                      <option value="shared">SHARED</option>
                    </select>
                    <div className="form-control-feedback">
                      <small>
                        The visibility of device type. Selecting "Shared" allows
                        members to use this device type in simulations.
                      </small>
                    </div>
                  </div>
                  <div className="form-group">
                    <label>Data Topic</label>
                    <input
                      type="text"
                      className="form-control form-control-line"
                      name="topic"
                      value={topic}
                      onChange={this.topicChange}
                      id="topic"
                    />
                    <div className="form-control-feedback">
                      <small>
                        The topic where individual sensor data is sent.
                      </small>
                    </div>
                  </div>
                  <div className="form-group">
                    <label>Data Transmission Duration</label>
                    <input
                      type="number"
                      className="form-control form-control-line"
                      name="duration"
                      value={duration}
                      onChange={this.durationChange}
                      id="duration"
                    />
                    <div className="form-control-feedback">
                      <small>
                        How long the device will simulate sending data to the
                        defined data topic (milliseconds) [must be >= 60000].
                      </small>
                    </div>
                  </div>
                  <div className="form-group">
                    <label>Data Transmission Interval</label>
                    <input
                      type="number"
                      className="form-control form-control-line"
                      name="interval"
                      value={interval}
                      onChange={this.intervalChange}
                      id="interval"
                    />
                    <div className="form-control-feedback">
                      <small>
                        How often devices will send data during a simulation
                        (milliseconds) [must be >= 1000].
                      </small>
                    </div>
                  </div>
                  <div className="form-group">
                    <label
                      style={{
                        marginBottom: "1px"
                      }}
                    >
                      Message Payload
                    </label>
                    <div className="form-control-feedback">
                      <small>
                        Define the message payload that will be simulated for
                        the device.
                      </small>
                    </div>
                  </div>
                  <div>
                    <label>Sample Message Payload</label>
                  </div>
                  <div className="m-t-20 m-b-20">
                    <button
                      type="button"
                      onClick={this.createDeviceType}
                      className="btn btn-success-alt btn-rounded m-r-15"
                    >
                      Save
                    </button>
                    <button
                      type="button"
                      className="btn btn-secondary btn-rounded"
                    >
                      Cancel
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
