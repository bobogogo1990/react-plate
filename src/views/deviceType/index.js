import React from "react";
import { connect } from "react-redux";
import "./index.less";
import { Pagination } from "antd";
import dayjs from "dayjs";
import Breadcrumb from "components/breadcrumb";

class DeviceType extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    devices: [],
    deviceCount: 0
  };

  async componentDidMount() {
    try {
      this.props.dispatch({ type: "SHOW_LOADING" });
      const [devices, deviceCount] = await Promise.all([
        awsapi.devicesTypes({ page: 0, op: "list" }),
        awsapi.devicesTypes({ op: "stats" })
      ]);
      this.setState({
        deviceCount: deviceCount.total,
        devices
      });
      this.props.dispatch({ type: "HIDE_LOADING" });
    } catch (error) {
      this.props.dispatch({ type: "HIDE_LOADING" });
    }
  }
  addDevice = () => {
    aws.pushWindow(this, "/securehome/deviceType/new");
  };
  render() {
    const data = this.state.devices;
    return (
      <div>
        <Breadcrumb activeLink={"DeviceType"} />
        <div className="row">
          <div className="col-lg-12">
            <div className="card">
              <div className="card-body">
                <button
                  type="button"
                  className="float-right btn btn-sm btn-success m-l-15"
                >
                  <i className="fa fa-refresh" /> Refresh
                </button>
                <button
                  type="button"
                  className="float-right btn btn-sm btn-success m-l-15"
                  onClick={this.addDevice}
                >
                  <i className="mdi mdi-plus" /> Add Device Type
                </button>
                <h4 className="card-title">
                  Device Types ( {this.state.deviceCount} )
                </h4>
                <div className="table-responsive m-t-20">
                  <table id="types-pagination" className="table stylish-table">
                    <thead>
                      <tr>
                        <th />
                        <th>Device Type</th>
                        <th className="text-right">Visibility</th>
                        <th className="text-right">Created At</th>
                        <th className="text-right">Last Updated At</th>
                        <th />
                      </tr>
                    </thead>
                    <tbody>
                      {data && data.length
                        ? data.map((v, i) => {
                            return (
                              <tr key={i}>
                                <td>
                                  <span className="round round-light-grey">
                                    <i
                                      className="mdi mdi-file-xml text-normal"
                                      style={{ fontSize: "24px" }}
                                    />
                                  </span>
                                </td>
                                <td>
                                  <h6> {v.name} </h6>
                                  <small className="text-muted">
                                    {" "}
                                    {v.typeId}{" "}
                                  </small>
                                </td>
                                <td className="text-right">
                                  <span
                                    className={
                                      v.visibility === "shared"
                                        ? "label label-light-success-alt"
                                        : "label label-light-danger"
                                    }
                                  >
                                    {v.visibility}
                                  </span>
                                </td>
                                <td className="text-right">
                                  {dayjs(v.createdAt).format("MMM DD YYYY")}
                                </td>
                                <td className="text-right">
                                  {dayjs(v.updatedAt).format("MMM DD YYYY")}
                                </td>
                                <td>
                                  <div className="pull-right" role="group">
                                    <button
                                      type="button"
                                      className="btn btn-success btn-sm"
                                      data-toggle="tooltip"
                                      title="Edit Device Type"
                                      data-placement="top"
                                    >
                                      <i className="mdi mdi-pencil" /> Edit
                                    </button>
                                  </div>
                                </td>
                              </tr>
                            );
                          })
                        : null}
                    </tbody>
                  </table>
                  <Pagination
                    defaultCurrent={1}
                    total={50}
                    style={{ float: "right" }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    showLoading: state.home.showLoading
  };
};

export default connect(mapStateToProps)(DeviceType);
