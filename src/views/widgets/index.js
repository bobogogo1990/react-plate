import React from "react";
import "../../assets/css/icons/material-design-iconic-font/css/materialdesignicons.min.css";
import Breadcrumb from "components/breadcrumb";

class Widgets extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            metrics: null,
            filterStages: ['< All >', 'provisioning', 'sleeping', 'stopping', 'running'],
            filterTypes: []
        };
    }

    async componentDidMount() {
        const metrics = await awsapi.deviceWidgets({
            op: "stats"
        });
        this.setState({ metrics });
    }

    render() {
        const { metrics, filterStages } = this.state;
        return (
            <div>
                <Breadcrumb activeLink={"widgets"} />
                <div className="row">
                    <div className="col-lg-3 col-md-6">
                        <div className="card">
                            <div className="card-body">
                                <div className="d-flex flex-row">
                                    <div className="round round-lg align-self-center round-muted"><i className="mdi mdi-chip"></i></div>
                                    <div className="m-l-10 align-self-center">
                                        <h3 className="m-b-0 font-light">{metrics ? metrics.total : ""}</h3>
                                        <h5 className="text-muted m-b-0">Total Widgets</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6">
                        <div className="card">
                            <div className="card-body">
                                <div className="d-flex flex-row">
                                    <div className="round round-lg align-self-center round-success-alt"><i className="mdi mdi-chip"></i></div>
                                    <div className="m-l-10 align-self-center">
                                        <h3 className="m-b-0 font-lgiht">{metrics ? metrics.hydrated : ""}</h3>
                                        <h5 className="text-muted m-b-0">Simulating</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6">
                        <div className="card">
                            <div className="card-body">
                                <div className="d-flex flex-row">
                                    <div className="round round-lg align-self-center round-warning"><i className="mdi mdi-chip"></i></div>
                                    <div className="m-l-10 align-self-center">
                                        <h3 className="m-b-0 font-lgiht">{metrics ? metrics.stopping : ""}</h3>
                                        <h5 className="text-muted m-b-0">Stopping</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6">
                        <div className="card">
                            <div className="card-body">
                                <div className="d-flex flex-row">
                                    <div className="round round-lg align-self-center round-danger"><i className="mdi mdi-chip"></i></div>
                                    <div className="m-l-10 align-self-center">
                                        <h3 className="m-b-0 font-lgiht">{metrics ? metrics.sleeping : ""}</h3>
                                        <h5 className="text-muted m-b-0">Sleeping</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">Filter</h4>
                                <div className="m-t-20">
                                    <form name="filterForm">
                                        <div className="row m-0">
                                            <div className="col-xs-12 col-md-4">
                                                <div className="form-group">
                                                    <label className="form-control-label">Device Type:</label>
                                                    <select className="form-control" name="subCategory" id="subCategory">
                                                        <option></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="col-xs-12 col-md-4">
                                                <div className="form-group">
                                                    <label className="form-control-label">Stage:</label>
                                                    <select className="form-control" name="stage" id="stage">
                                                        {
                                                            filterStages ? filterStages.map((v, i) => {
                                                                return <option key={i}>v</option>
                                                            }) : null}
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="col-xs-12 col-md-4">
                                                <label className="form-control-label">Device ID:</label>
                                                <input type="text" className="form-control" name="deviceId" id="deviceId" />
                                            </div>
                                        </div>
                                        <button type="submit" className="float-right btn btn-sm btn-success"><i className="mdi mdi-magnify"></i> Filter</button>
                                        <button type="submit" className="float-right btn btn-sm btn-success m-r-10"><i className="mdi mdi-close"></i> Clear</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card">
                            <div className="card-body">
                                <button type="button" className="float-right btn btn-sm btn-success m-l-15"><i className="fa fa-refresh"></i> Refresh</button>
                                <button type="button" className="float-right btn btn-sm btn-success m-l-15"><i className="mdi mdi-plus"></i> Add Widget</button>
                                <button type="submit" className="float-right btn btn-sm btn-danger m-l-15"><i className="mdi mdi-stop"></i> Stop Devices</button>
                                <button type="submit" className="float-right btn btn-sm btn-success"><i className="mdi mdi-play"></i> Start Devices</button>
                                <div className="modal fade" id="addModal">
                                    <div className="modal-dialog" role="document">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <h4 className="modal-title" id="addModalLabel1">Create a widget</h4>
                                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div className="modal-body">
                                                <form name="addForm" className="form-control-line">
                                                    <div className="form-group">
                                                        <label className="form-control-label">Device Type:</label>
                                                        <select className="form-control" name="type" id="type">
                                                            <option> t.name</option>
                                                        </select>
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="form-control-label">Number of widgets:</label>
                                                        <input type="number" className="form-control form-control-line" />
                                                        <div className="form-control-feedback">
                                                            <small>Number of widgets to create based on the device type. Up to 100 devices.</small>
                                                        </div>
                                                    </div>
                                                    <div className="alert alert-danger">
                                                        <h3 className="text-danger">
                                                            <i className="fa fa-exclamation-circle"></i> Oops!
                                                        </h3>
                                                    </div>
                                                </form>
                                            </div>
                                            <div className="modal-footer">
                                                <button type="button" className="btn btn-default">Cancel</button>
                                                <button type="submit" className="btn btn-success">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h4 className="card-title">Widgets <span></span></h4>
                                <div className="table-responsive m-t-10">
                                    <table id="devices-pagination" className="table stylish-table" >
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th><input type="checkbox" id="chk_all" className="classNameic-checkbox" /></th>
                                                <th>Device</th>
                                                <th className="text-right">Stage</th>
                                                <th className="text-right">Simulations</th>
                                                <th className="text-right">Last Simulation</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <span className="round round-light-grey">
                                                        <i className="mdi mdi-chip text-normal"></i>
                                                        <div> <span className="heartbit"></span> <span className="point"></span> </div>
                                                    </span>
                                                </td>
                                                <td><input type="checkbox" id="chk_device.id" className="classNameic-checkbox" /></td>
                                                <td>
                                                    <h6>device.subCategory</h6>
                                                    <small className="text-muted" style={{ textTransform: "none" }}>device.id</small>
                                                </td>
                                                <td className="text-right">
                                                    <span>device.stage === 'hydrated' ? 'running' : device.stage</span>
                                                </td>
                                                <td className="text-right"><span className="label label-success">device.runs</span></td>
                                                <td className="text-right">formatDate(device.endedAt)</td>
                                                <td>
                                                    <div className="pull-right" role="group" aria-label="Basic example">
                                                        <button type="button"><i className="mdi mdi-information"></i> View</button>
                                                        <button type="button"><i className="mdi mdi-play"></i> Start</button>
                                                        <button type="button"><i className="mdi mdi-stop"></i> Stop</button>
                                                        <button type="button"><i className="mdi mdi-delete-circle"></i> Delete</button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div >
                        </div >
                    </div >
                </div >
            </div >
        );
    }
}

export default Widgets;
