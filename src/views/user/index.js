import React from "react";
import Breadcrumb from "components/breadcrumb";

export default class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userInfo: null
    };
  }
  async componentDidMount() {
    const idToken = await aws.getIdToken();
    const userInfo = await awsapi.getUserInfo(idToken);
    this.setState({ userInfo })
  }
  render() {
    const user = this.state.userInfo && this.state.userInfo.length && this.state.userInfo[0];
    return <div>
      <Breadcrumb activeLink={"User"} />
      <div className="row">
        <div className="col-lg-12">
          <div className="card">
            <div className="card-body">
              <button type="button" className="float-right btn btn-sm btn-success m-l-15"><i className="fa fa-refresh"></i> Refresh</button>
              <button type="button" className="float-right btn btn-sm btn-success"><i className="mdi mdi-account-plus"></i> Invite User</button>
              <h4 className="card-title">Users</h4>
              <div className="table-responsive m-t-20">
                <table id="users-pagination" className="table stylish-table">
                  <thead>
                    <tr>
                      <th></th>
                      <th>User Id</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Enabled</th>
                      <th>Status</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      user ? <tr>
                        <td><span className="round round-success"><i className="mdi mdi-account"></i></span></td>
                        <td>
                          <h6>{user.user_id}</h6>
                        </td>
                        <td>{user.name}</td>
                        <td>{user.email}</td>
                        <td><span className={user.enabled ? 'label label-rounded label-info' : 'label label-rounded label-danger'}>{user.enabled + ''}</span></td>
                        <td><span className={user.status === 'CONFIRMED' ? 'label label-light-info' : 'label label-light-danger'}>{user.status}</span></td>
                        <td>
                          <button type="button" className="pull-right btn btn-sm btn-success btn-rounded"><i className="mdi mdi-account-edit"></i> Review</button>
                        </td>
                      </tr> : null
                    }
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div >
      </div >
    </div >
  }
}
