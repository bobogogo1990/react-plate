import React from "react";
import { connect } from "react-redux";
import "./index.less";
import "../../assets/css/icons/material-design-iconic-font/css/materialdesignicons.min.css";
import { Button, Carousel, Input } from "antd";
import { setLocalData } from "../../utils/publicMethods";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
    this.doLogin = this.doLogin.bind(this);
  }

  changeUsername = ({ target = {} }) => {
    this.setState({
      username: target.value
    });
  };

  changePassword = ({ target = {} }) => {
    this.setState({
      password: target.value
    });
  };

  async doLogin() {
    const { username, password } = this.state;
    this.props.dispatch({ type: "SHOW_LOADING" });
    try {
      const res = await aws.authenticate(username, password);
      console.log("res = ", res);
      const idToken = await aws.getIdToken();
      const profile = await awsapi.getProfileInfo(idToken);
      console.log("profile = ", profile);
      aws.setLocalData({ profile });
      aws.pushWindow(this, "/");
      this.props.dispatch({ type: "HIDE_LOADING" });
    } catch (error) {
      console.log("error = ", error);
      this.props.dispatch({ type: "HIDE_LOADING" });
    }
  }

  render() {
    return (
      <div className="login-container">
        <div className="lblock">
          <div className="login-logo">
            <img src="../../assets/img/aws_smile.png" height="25" />
            <h5>IoT Device Simulator</h5>
          </div>
          <div className="brand">
            <span
              className="mdi mdi-lock-pattern"
              style={{ fontSize: "200px", color: "#fff" }}
            />
          </div>
          <div className="carousel">
            <Carousel autoplay>
              <div>
                <h3 className="mb-3 text-white">Device Simulation Console</h3>
                <p>
                  Create, monitor, and manage your connected devices with a few
                  simple clicks in the simulation console.
                </p>
              </div>
              <div>
                <h3 className="mb-3 text-white">
                  Customize your device collection
                </h3>
                <p>
                  Leverage existing industry modules, such as Automotive, or
                  customize your own device payloads for your device
                  collections.
                </p>
              </div>
              <div>
                <h3 className="mb-3 text-white">Real-time Performance Data</h3>
                <p>
                  View real-time performance and telemetry data for console
                  displays of each device in your collection.
                </p>
              </div>
            </Carousel>
          </div>
        </div>
        <div className="rblock">
          <div className="form">
            <h4>SIGN IN TO YOUR ACCOUNT</h4>
            <div className="form-item">
              <Input
                placeholder="Your email"
                value={this.state.username}
                onChange={this.changeUsername}
              />
            </div>
            <div className="form-item">
              <Input.Password
                placeholder="Your password"
                value={this.state.password}
                onChange={this.changePassword}
              />
            </div>
            <Button className="submitbtn" onClick={this.doLogin}>
              Sign in
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}
export default connect(mapStateToProps)(Login);
