import React from "react";
import { connect } from "react-redux";
import "./index.less";
import { BrowserRouter as Router, Link } from "react-router-dom";
import { renderRoutes } from "react-router-config";
import AWSHeader from "../../components/header";
import { Layout, Menu, Icon, Spin } from "antd";
const { Content, Sider } = Layout;
const MenuItemGroup = Menu.ItemGroup;

class App extends React.Component {
    constructor(props) {
        super(props);
    }
    state = {
        collapsed: false,
        toggleStyle: null
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
            toggleStyle: !this.state.collapsed
                ? { display: "none" }
                : { display: "inline-block" }
        });
    };

    fetchData(params) {
        console.log("params = ", params);
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve([
                    {
                        deviceType: "phone",
                        type: "apple",
                        location: [39.0, 127.0]
                    }
                ]);
            }, 1000);
        });
    }

    componentDidMount() {
        // 发送请求拿数据
    }

    render() {
        return (
            <Router>
                <Spin spinning={this.props.showLoading}>
                    <Layout style={{ height: "100%" }}>
                        <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
                            <div className="navbar-header" style={{ width: "inherit" }}>
                                <a className="navbar-brand">
                                    <img
                                        alt="homepage"
                                        className="light-logo"
                                        src={"/assets/img/aws_smile.png"}
                                        width="30px"
                                    />
                                    <span className="text" style={this.state.toggleStyle}>
                                        <img
                                            alt="homepage"
                                            className="light-logo"
                                            height="40"
                                            src="/assets/img/simengine.png"
                                        />
                                    </span>
                                </a>
                            </div>
                            <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
                                <Menu.Item key="1">
                                    <Icon type="home" />
                                    <span>
                                        <Link to="/securehome">Home</Link>
                                    </span>
                                </Menu.Item>
                                <MenuItemGroup key="g1" title="MY MENU">
                                    <Menu.Item key="2">
                                        <Icon type="dashboard" />
                                        <span>
                                            <Link to="/securehome/dashboard">Dashboard</Link>
                                        </span>
                                    </Menu.Item>
                                    <Menu.Item key="3">
                                        <Icon type="table" />
                                        <span>
                                            <Link to="/securehome/device">My device</Link>
                                        </span>
                                    </Menu.Item>
                                    <Menu.Item key="4">
                                        <Icon type="profile" />
                                        <span>
                                            <Link to="/securehome/profile">My profile</Link>
                                        </span>
                                    </Menu.Item>
                                </MenuItemGroup>
                                <MenuItemGroup key="g2" title="MODULES">
                                    <Menu.Item key="5">
                                        <Icon type="dashboard" />
                                        <span>
                                            <Link to="/securehome/deviceType">Device Types</Link>
                                        </span>
                                    </Menu.Item>
                                    <Menu.Item key="6">
                                        <Icon type="table" />
                                        <span>
                                            <Link to="/securehome/widgets">Widgets</Link>
                                        </span>
                                    </Menu.Item>
                                    <Menu.Item key="7">
                                        <Icon type="car" />
                                        <span>
                                            <Link to="/securehome/automotive">Automotive</Link>
                                        </span>
                                    </Menu.Item>
                                </MenuItemGroup>
                                <MenuItemGroup key="g3" title="ADMINISTRATION">
                                    <Menu.Item key="8">
                                        <Icon type="user" />
                                        <span>
                                            <Link to="/user">Users</Link>
                                        </span>
                                    </Menu.Item>
                                    <Menu.Item key="9">
                                        <Icon type="setting" />
                                        <span>
                                            <Link to="/setting">Settings</Link>
                                        </span>
                                    </Menu.Item>
                                </MenuItemGroup>
                            </Menu>
                        </Sider>
                        <Layout>
                            <AWSHeader
                                toggle={this.toggle}
                                collapsed={this.state.collapsed}
                            />
                            <Content
                                style={{
                                    background: "#eef5f9",
                                    paddingBottom: "60px",
                                    marginTop: "64px",
                                    minHeight: "auto",
                                    flex: 1
                                }}
                            >
                                <div className="container-fluid">
                                    {renderRoutes(this.props.route.routes)}
                                </div>
                            </Content>
                        </Layout>
                    </Layout>
                </Spin>
            </Router>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        showLoading: state.home.showLoading
    };
};

export default connect(mapStateToProps)(App);
