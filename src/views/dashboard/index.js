import React from "react";
import "./index.less";
import Breadcrumb from "components/breadcrumb";

class Dashboard extends React.Component {
    state = {
    };

    async componentDidMount() {

    }

    render() {
        return <div>
            <Breadcrumb activeLink={"Dashboard"} />
            <div className="row">
                <div className="col-lg-4 col-md-12">
                    <div className="card card-inverse card-info">
                        <div className="card-body">
                            <div className="d-flex">
                                <div className="m-r-20 align-self-center">
                                    <h1 className="text-white"><i className="mdi mdi-av-timer"></i></h1></div>
                                <div>
                                    <h3 className="card-title">Total Simulations</h3>
                                    <h6 className="card-subtitle"></h6> </div>
                            </div>
                            <div className="row">
                                <div className="col-5 align-self-center">
                                    <h2 className="font-light text-white"> runs</h2>
                                </div>
                                <div className="col-7 p-t-10 p-b-20 align-self-center">
                                    <div className="usage chartist-chart" style={{ height: "65px" }}></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card card-inverse card-success">
                        <div className="card-body">
                            <div className="d-flex">
                                <div className="m-r-20 align-self-center">
                                    <h1 className="text-white"><i className="mdi mdi-clock-fast"></i></h1></div>
                                <div>
                                    <h3 className="card-title">Total Simulation Minutes</h3>
                                    <h6 className="card-subtitle"></h6> </div>
                            </div>
                            <div className="row">
                                <div className="col-5 align-self-center">
                                    <h2 className="font-light text-white">min</h2>
                                </div>
                                <div className="col-7 p-t-10 p-b-20 text-right">
                                    <div className="spark-count" style={{ height: "65px" }}></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-4 col-md-12">
                    <div className="card">
                        <div className="card-body">
                            <h3 className="card-title">Simulation Runs</h3>
                            <h6 className="card-subtitle"></h6>
                            <div id="runs" style={{ height: "260px", width: "100%" }}></div>
                        </div>
                        <div>
                            <hr className="m-t-0 m-b-0" />
                        </div>
                        <div className="card-body text-center ">
                            <ul className="list-inline m-b-0">
                                <li>
                                    <h6 className="text-muted"> total device types used</h6>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="col-lg-4 col-md-12">
                    <div className="card">
                        <div className="card-body">
                            <h3 className="card-title">Device Breakdown</h3>
                            <h6 className="card-subtitle"></h6>
                            <table className="table browser m-t-30 no-border" style={{ marginBottom: "0px" }}>
                                <tbody>
                                    <tr>
                                        <td style={{ width: "40px" }}><span className="round round-light-grey"><i className="mdi mdi-car text-normal"></i></span></td>
                                        <td>--</td>
                                        <td className="text-right"><span className="label label-light-info">---</span></td>
                                    </tr>
                                    <tr>
                                        <td colSpan="2" style={{ fontWeight: "800" }}>Total Devices</td>
                                        <td className="text-right"><span className="label label-light-success-alt">--</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <div className="card">
                        <div className="row">
                            <div className="col-lg-9 col-md-7">
                                <div className="card-body">
                                    <h3>My simulation minutes (last 6 months)</h3>
                                    <h6 className="card-subtitle">--</h6>
                                    <div className="demo-container" style={{ height: "400px}" }} >
                                        <div className="sim-hours" style={{ height: "400px}" }}></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-5 b-l p-l-0">
                                <div className="m-t-30 m-b-40 p-t-20 p-b-20 text-center">
                                    <font className="display-3">--</font>
                                    <h6 className="text-muted">Total Simulation Minutes</h6>
                                    <a className="btn btn-success" href="securehome/devices">View My Devices</a>
                                </div>
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col p-r-0">
                                            <h1 className="font-light">--<span style={{ fontSize: "20" }}>min</span></h1>
                                            <h6 className="text-muted">Automotive</h6></div>
                                        <div className="col text-right align-self-center">
                                            <div className="round align-self-center round-info"><i className="mdi mdi-car" style={{ fontSize: "2em" }}></i></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col p-r-0">
                                            <h1 className="font-light">--<span style={{ fontSize: "20" }}>min</span></h1>
                                            <h6 className="text-muted">Generic</h6></div>
                                        <div className="col text-right align-self-center">
                                            <div className="round align-self-center round-success"><i className="mdi mdi-chip" style={{ fontSize: "2em" }}></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}
export default Dashboard;
