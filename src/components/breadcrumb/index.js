import React from "react";
export default class Breadcrumb extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      widgets: {
        total: 0
      }
    };
  }

  async componentDidMount() {
    const widgets = await awsapi.deviceWidgets({
      op: "stats"
    });
    this.setState({ widgets });
  }

  render() {
    const { widgets } = this.state;
    return (
      <div className="row page-titles">
        <div className="col-md-5 col-8 align-self-center">
          <h3 id="view-title">title</h3>
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="/securehome">Home</a>
            </li>
            <li className="breadcrumb-item active">{this.props.activeLink}</li>
          </ol>
        </div>
        <div className="col-md-7 col-4 align-self-center">
          <div className="d-flex m-t-10 justify-content-end">
            <div className="d-flex m-r-20 m-l-10 hidden-md-down">
              <div className="chart-text m-r-10">
                <h6 className="m-b-0">
                  <small>Devices</small>
                </h6>
                <h4 className="m-t-0 text-success">{widgets.total} total</h4>
              </div>
            </div>
            <div className="d-flex m-r-20 m-l-10 hidden-md-down">
              <div className="chart-text m-r-10">
                <h6 className="m-b-0">
                  <small>Simulations</small>
                </h6>
                <h4 className="m-t-0 text-success-alt">
                  {widgets.hydrated} running
                </h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
