import React from "react";
import { Layout, Icon, Popover } from 'antd';
const { Header } = Layout;
import "./index.less";

class AWSHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: {}
    };
  }
  viewProfile = () => {

  }
  componentDidMount() {
    const profile = aws.getLocalData("profile");
    this.setState({
      profile: profile ? JSON.parse(profile) : {}
    });
  }
  render() {
    const profile = this.state.profile;
    const content = (<div>
      <ul className="user-dropdown">
        <li>
          <div>
            <div style={{ textAlign: "center", fontSize: "50px" }}>
              <i className="fa fa-user-circle"></i>
            </div>
            <div className="u-text">
              <h4>{profile.name}</h4>
              <p className="text-muted">{profile.email}</p>
              <a className="btn btn-rounded btn-danger btn-sm" href="/securehome/profile">
                <i className="mdi mdi-account-card-details" onClick={this.viewProfile}></i> View Profile
              </a>
            </div>
          </div>
        </li>
        <li className="divider"></li>
        <li className="li-route"><a href="/securehome/device"><i className="mdi mdi-view-module"></i> My Devices</a></li>
        <li className="divider"></li>
        <li className="li-route"><a href="/logout"><i className="fa fa-power-off"></i> Logout</a></li>
      </ul>
    </div>);
    return <Header
      style={{
        position: "fixed",
        width: "100%",
        padding: 0,
        zIndex: 999,
        color: "#fff",
        backgroundColor: "#001529"
      }}>
      <Icon
        className="trigger"
        type={this.props.collapsed ? "menu-unfold" : "menu-fold"}
        onClick={this.props.toggle}
      />
      <Popover placement="bottomRight" content={content} trigger="click">
        <i className="fa fa-user-circle-o"></i>
      </Popover>
    </Header>
  }
}

export default AWSHeader;