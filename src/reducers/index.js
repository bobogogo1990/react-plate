import { combineReducers } from "redux";

const initHomeState = {
  showLoading: false
};

const home = (state = initHomeState, action) => {
  switch (action.type) {
    case "SHOW_LOADING":
      return {
        showLoading: true
      };
    case "HIDE_LOADING":
      return {
        showLoading: false
      };
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  home
});

export default rootReducer;
