import fetch from "./api.config";

async function request(params = {}) {
  // string will set url
  if (typeof params == "string") {
    params = {
      url: params
    };
  }
  const idToken = await aws.getIdToken();
  if (idToken) {
    params.headers = Object.assign({ Authorization: idToken }, params.headers);
  }

  return fetch(params);
}

export function getProfileInfo() {
  // 获取用户信息
  return request("/profile");
}

export function deviceWidgets(params = {}) {
  return request({
    url: "/devices/widgets",
    params
  });
}

export function devicesTypes(params = {}) {
  return request({
    url: "/devices/types",
    params
  });
}

export function getUserInfo(idToken) {
  // 获取用户信息
  return request("/admin/users");
}

export function createDeviceType(data = {}) {
  return request({
    url: "/devices/types",
    method: "POST",
    data
  });
}
