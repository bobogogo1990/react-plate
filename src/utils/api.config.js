import axios from "axios";
const BASE_URL = "https://3yg3c5fwo6.execute-api.us-east-1.amazonaws.com/prod";

const service = axios.create({
  // api的base_url
  timeout: 100000, // 请求超时时间
  headers: {
    "Content-Type": "application/json;charset=UTF-8"
  },
  method: "GET",
  data: {}
});

service.interceptors.request.use(
  config => {
    config.url = BASE_URL + config.url;

    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

service.interceptors.response.use(
  res => {
    console.log("api config res = ", res);
    return res.data;
  },
  err => {}
);

export default service;
