import {
  pushWindow,
  authenticate,
  setLocalData,
  getLocalData,
  getIdToken,
  changePassword
} from "./publicMethods";

import {
  getUserInfo,
  deviceWidgets,
  devicesTypes,
  getProfileInfo,
  createDeviceType
} from "./apis";

window.aws = {
  pushWindow,
  authenticate,
  setLocalData,
  getLocalData,
  getIdToken,
  changePassword
};

window.awsapi = {
  getProfileInfo,
  getUserInfo,
  deviceWidgets,
  devicesTypes,
  createDeviceType
};
