import {
  AuthenticationDetails,
  CognitoUser,
  CognitoUserPool
} from "amazon-cognito-identity-js";
import * as AWS from "aws-sdk/global";

const appVariables = {
  IOT_ENDPOINT: "a3andfy8nqsdy6-ats.iot.us-east-1.amazonaws.com",
  COGNITO_IDP_ENDPOINT: "",
  COGNITO_IDENTITY_ENDPOINT: "",
  APIG_ENDPOINT: "https://3yg3c5fwo6.execute-api.us-east-1.amazonaws.com/prod",
  PRINCIPAL_POLICY: "xue3-IotDeviceSimPrincipalPolicy-J1FCFIB6Q4",
  IDENTITY_POOL_ID: "us-east-1:8b3f7552-46a8-48f0-a138-acdf118ed55d",
  USER_POOL_ID: "us-east-1_jJyATRGPi",
  USER_POOL_CLIENT_ID: "2b1jbo0brn1dihb1h8k2sv5dbt",
  REGION: "us-east-1"
};

export function pushWindow(env, path) {
  env.props.history.push(path);
}

export function getUserPool() {
  return new CognitoUserPool({
    UserPoolId: appVariables.USER_POOL_ID,
    ClientId: appVariables.USER_POOL_CLIENT_ID,
    Paranoia: 8
  });
}

export function getIdToken() {
  return new Promise((resolve, reject) => {
    getCurrentUser().getSession((err, session) => {
      if (err) return reject(err);
      const token = session.getIdToken().getJwtToken();
      resolve(token);
    });
  });
}

export function getAccessToken() {
  return new Promise((resolve, reject) => {
    getCurrentUser().getSession((err, session) => {
      if (err) return reject(err);
      const token = session.getAccessToken().getJwtToken();
      resolve(token);
    });
  });
}

export function getCurrentUser() {
  return getUserPool().getCurrentUser();
}

export function setLocalData(obj) {
  const keys = Object.keys(obj);
  keys.forEach(k => {
    delete window.localStorage[k];
    window.localStorage[k] = JSON.stringify(obj[k]);
  });
}

export function getLocalData(k) {
  if (!k) return "";
  return window.localStorage[k];
}

export function authenticate(username, password, callback) {
  console.info("UserLoginService: starting the authentication");

  const authenticationData = {
    Username: username,
    Password: password
  };

  const authenticationDetails = new AuthenticationDetails(authenticationData);

  const userData = {
    Username: username,
    Pool: getUserPool()
  };

  console.info("UserLoginService: Params set...Authenticating the user");

  const cognitoUser = new CognitoUser(userData);

  console.info("UserLoginService: config is " + JSON.stringify(AWS.config));
  const _self = this;
  return new Promise((resolve, reject) => {
    cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: function(result) {
        console.info(
          "In authenticateUser onSuccess callback result = ",
          result
        );
        resolve(result);
      },
      onFailure: function(err) {
        // callback.cognitoCallback(err.message, null);
        console.log(err);
        reject(err);
      }
    });
  });
}

export function changePassword(old, current) {
  const currentUser = getCurrentUser();

  return new Promise((resolve, reject) => {
    if (!currentUser) return reject("current user not exist");

    currentUser.getSession((err, session) => {
      // user not exist error
      if (err) return reject("current user not exist");
      if (!session.isValid()) return reject("current user not exist");

      // start change password
      console.log("start old, current = ", old, "   ", current);
      currentUser.changePassword(old, current, (err, result) => {
        if (err) return reject("change password failed");
        // change success
        resolve(result);
      });
    });
  });
}
