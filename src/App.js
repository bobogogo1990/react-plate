import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { renderRoutes } from "react-router-config";
import routes from "./routes";
import Login from "./views/login";
import configureStore from "./store/configureStore";
import { Provider } from "react-redux";

const store = configureStore();

class App extends React.Component {
  state = {
    inapp: false
  };
  render() {
    return (
      <Router>
        <Provider store={store}>{renderRoutes(routes)}</Provider>
      </Router>
    );
  }
}

export default App;
