import Root from "./views/root";
import Home from "./views/home";
import Login from "./views/login";
import Dashboard from "./views/dashboard";
import Device from "./views/myDevice";
import Profile from "./views/profile";
import DeviceType from "./views/deviceType";
import Widgets from "./views/widgets";
import Automotive from "./views/automotive";
import NewDeviceType from "./views/deviceType/newdevice";
import User from "./views/user";
import Settings from "./views/setting";

export default [
  {
    component: Root,
    routes: [
      {
        path: "/",
        exact: true,
        component: Home
      },
      {
        path: "/securehome",
        exact: true,
        component: Home
      },
      {
        path: "/securehome/dashboard",
        exact: true,
        component: Dashboard
      },
      {
        path: "/securehome/device",
        exact: true,
        component: Device
      },
      {
        path: "/securehome/profile",
        exact: true,
        component: Profile
      },
      {
        path: "/securehome/deviceType",
        exact: true,
        component: DeviceType
      },
      {
        path: "/securehome/deviceType/new",
        exact: true,
        component: NewDeviceType
      },
      {
        path: "/securehome/widgets",
        exact: true,
        component: Widgets
      },
      {
        path: "/securehome/automotive",
        exact: true,
        component: Automotive
      },
      {
        path: "/user",
        exact: true,
        component: User
      },
      {
        path: "/setting",
        exact: true,
        component: Settings
      },
      {
        path: "/login",
        component: Login
      },
      {
        path: "/logout",
        component: Login
      }
    ]
  }
];
