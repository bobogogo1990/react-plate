import "./style.less";
import React from "react";
import { hydrate } from "react-dom";
import { hot } from "react-hot-loader/root";
import "./utils/aws";
import App from "./App";
const HotApp = hot(App);

hydrate(<HotApp />, document.getElementById("app"));
